package com.poc.drilling.model;

import java.util.List;

public class CalculateCount {

    private String sslipdx;
    private List<WobaxRpm> wobaxRpmaxAry;
    private int count;

    public CalculateCount(String sslipdx, List<WobaxRpm> wobaxRpmaxAry, int count) {
        this.sslipdx = sslipdx;
        this.wobaxRpmaxAry = wobaxRpmaxAry;
        this.count = count;
    }

    public List<WobaxRpm> getWobaxRpmaxAry() {
        return wobaxRpmaxAry;
    }

    public void setWobaxRpmaxAry(List<WobaxRpm> wobaxRpmaxAry) {
        this.wobaxRpmaxAry = wobaxRpmaxAry;
    }

    public String getSslipdx() {
        return sslipdx;
    }

    public void setSslipdx(String sslipdx) {
        this.sslipdx = sslipdx;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
