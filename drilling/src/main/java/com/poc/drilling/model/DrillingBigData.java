package com.poc.drilling.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;


@Component
@Document(indexName = "drilling-big-data", type = "DrillingBigData", shards=6)
public class DrillingBigData {

    @Id
    private Long id;

    private String  sslipdx;

    private String  wobax;

    private String  rpmax;

    private String ss;

    //COP_MSE
    private String  copMse;

    private String torax;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSslipdx() {
        return sslipdx;
    }

    public void setSslipdx(String sslipdx) {
        this.sslipdx = sslipdx;
    }

    public String getWobax() {
        return wobax;
    }

    public void setWobax(String wobax) {
        this.wobax = wobax;
    }

    public String getRpmax() {
        return rpmax;
    }

    public void setRpmax(String rpmax) {
        this.rpmax = rpmax;
    }

    public String getSs() {
        return ss;
    }

    public void setSs(String ss) {
        this.ss = ss;
    }

    public String getCopMse() {
        return copMse;
    }

    public void setCopMse(String copMse) {
        this.copMse = copMse;
    }

    public String getTorax() {
        return torax;
    }

    public void setTorax(String torax) {
        this.torax = torax;
    }
}
