package com.poc.drilling.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;


@Component
@Document(indexName = "drilling-low-data", type = "DrillingLowData", shards=6)
public class DrillingLowData {

    @Id
    private Long id;

    private String  sslipdx;

    private String  wobax;

    private String  rpmax;

    private String ss;

    //COP_MSE
    private String  copMse;

    private String torax;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSslipdx() {
        return sslipdx;
    }

    public void setSslipdx(String sslipdx) {
        this.sslipdx = sslipdx;
    }

    public String getWobax() {
        return wobax;
    }

    public DrillingLowData setWobax(String wobax) {
        this.wobax = wobax;
        return this;
    }

    public String getRpmax() {
        return rpmax;
    }

    public DrillingLowData setRpmax(String rpmax) {
        this.rpmax = rpmax;
        return this;
    }

    public String getSs() {
        return ss;
    }

    public void setSs(String ss) {
        this.ss = ss;
    }

    public String getCopMse() {
        return copMse;
    }

    public void setCopMse(String copMse) {
        this.copMse = copMse;
    }

    public String getTorax() {
        return torax;
    }

    public void setTorax(String torax) {
        this.torax = torax;
    }
}
