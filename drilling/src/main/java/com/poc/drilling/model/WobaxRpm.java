package com.poc.drilling.model;

public class WobaxRpm {

    private String wobax;
    private String rpmax;

    public WobaxRpm(String wobax, String rpmax) {
        this.wobax = wobax;
        this.rpmax = rpmax;
    }

    public String getWobax() {
        return wobax;
    }

    public void setWobax(String wobax) {
        this.wobax = wobax;
    }

    public String getRpmax() {
        return rpmax;
    }

    public void setRpmax(String rpmax) {
        this.rpmax = rpmax;
    }
}
