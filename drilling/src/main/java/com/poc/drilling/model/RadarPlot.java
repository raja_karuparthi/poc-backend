package com.poc.drilling.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;

@Component
@Document(indexName = "radar-plot", type = "RadarPlot", shards = 6)
public class RadarPlot {
    // Cuttings Injection,LWD Acquistion,Differential Sticking,Hole Stability,Hole Cleaning,Stickslip,Lost Returns,Shaker Capacity

    @Id
    private Long id;
    private String cuttingsInjection;
    private String lwdAcquistion;
    private String differentialSticking;
    private String holeStability;
    private String holeCleaning;
    private String stickSlip;
    private String lostReturns;
    private String shakerCapacity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCuttingsInjection() {
        return cuttingsInjection;
    }

    public void setCuttingsInjection(String cuttingsInjection) {
        this.cuttingsInjection = cuttingsInjection;
    }

    public String getLwdAcquistion() {
        return lwdAcquistion;
    }

    public void setLwdAcquistion(String lwdAcquistion) {
        this.lwdAcquistion = lwdAcquistion;
    }

    public String getStickSlip() {
        return stickSlip;
    }

    public void setStickSlip(String stickSlip) {
        this.stickSlip = stickSlip;
    }

    public String getDifferentialSticking() {
        return differentialSticking;
    }

    public void setDifferentialSticking(String differentialSticking) {
        this.differentialSticking = differentialSticking;
    }

    public String getHoleStability() {
        return holeStability;
    }

    public void setHoleStability(String holeStability) {
        this.holeStability = holeStability;
    }

    public String getHoleCleaning() {
        return holeCleaning;
    }

    public void setHoleCleaning(String holeCleaning) {
        this.holeCleaning = holeCleaning;
    }

    public String getLostReturns() {
        return lostReturns;
    }

    public void setLostReturns(String lostReturns) {
        this.lostReturns = lostReturns;
    }

    public String getShakerCapacity() {
        return shakerCapacity;
    }

    public void setShakerCapacity(String shakerCapacity) {
        this.shakerCapacity = shakerCapacity;
    }
}
