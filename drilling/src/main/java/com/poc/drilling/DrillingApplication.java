package com.poc.drilling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableElasticsearchRepositories(basePackages = "com.poc.drilling.repository")
public class DrillingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrillingApplication.class, args);
	}

}
