package com.poc.drilling.jpa;

import com.poc.drilling.model.DrillingBigData;
import com.poc.drilling.model.DrillingLowData;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DrillingLowRepository extends ElasticsearchCrudRepository<DrillingLowData, Float> {
}
