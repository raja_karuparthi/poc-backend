package com.poc.drilling.jpa;

import com.poc.drilling.model.RadarPlot;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RadarPlotRepository extends ElasticsearchCrudRepository<RadarPlot, Long> {
}
