package com.poc.drilling.jpa;

import com.poc.drilling.model.DrillingBigData;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DrillingBigRepository extends ElasticsearchCrudRepository<DrillingBigData, Float> {
}
