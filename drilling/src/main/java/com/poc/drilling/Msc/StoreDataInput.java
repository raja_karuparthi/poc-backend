package com.poc.drilling.Msc;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class StoreDataInput {

    private String quantity;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
