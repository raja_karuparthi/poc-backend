package com.poc.drilling.serviceImpl;

import com.opencsv.CSVReader;
import com.poc.drilling.model.DrillingBigData;
import com.poc.drilling.model.DrillingLowData;
import com.poc.drilling.model.RadarPlot;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CsvReaderImpl {

    private static List<String> radarHeader = Arrays.asList("Cuttings Injection", "LWD Acquistion", "Differential Sticking",
            "Hole Stability", "Hole Cleaning", "Stickslip", "Lost Returns", "Shaker Capacity");

    private static List<String> headersReq = Arrays.asList("RPMAX", "WOBAX", "SSLIPDX", "SS", "COP_MSE", "TORAX");

    List<RadarPlot> getRadarPlotData(String filePath) {
        CSVReader reader;
        HashMap<String, Integer> headerMap = new HashMap<>();
        List<RadarPlot> radarPlotDataList = new ArrayList<>();
        try {
            reader = new CSVReader(new FileReader(filePath));
            int i = 0;
            RadarPlot eachRadarPlotAttr = null;
            for (String[] strings : reader.readAll()) {
                if (i == 0) {
                    // Assigning headers
                    for (int j = 0; j < strings.length; j++) {
                        String contents = strings[j].trim();
                        contents = removeUnwantedSpaces(contents); //65279 remove the character
                        if (radarHeader.contains(contents.trim())) {
                            System.out.println(contents+"----"+radarHeader);
                            headerMap.put(contents, j);
                        }
                    }
                }
                if (i > 0) {
                    //Cuttings Injection", "LWD Acquistion", "Differential Sticking",
                    //            "Hole Stability", "Hole Cleaning" ,"Stickslip", "Lost Returns", "Shaker Capacity"
                    eachRadarPlotAttr = new RadarPlot();
                    eachRadarPlotAttr.setCuttingsInjection(strings[headerMap.get("Cuttings Injection")]);
                    eachRadarPlotAttr.setLwdAcquistion(strings[headerMap.get("LWD Acquistion")]);
                    eachRadarPlotAttr.setDifferentialSticking(strings[headerMap.get("Differential Sticking")]);
                    eachRadarPlotAttr.setHoleStability(strings[headerMap.get("Hole Stability")]);
                    eachRadarPlotAttr.setHoleCleaning(strings[headerMap.get("Hole Cleaning")]);
                    eachRadarPlotAttr.setStickSlip(strings[headerMap.get("Stickslip")]);
                    eachRadarPlotAttr.setLostReturns(strings[headerMap.get("Lost Returns")]);
                    eachRadarPlotAttr.setShakerCapacity(strings[headerMap.get("Shaker Capacity")]);
                    radarPlotDataList.add(eachRadarPlotAttr);
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(radarPlotDataList);
        return radarPlotDataList;
    }


    // get radar plot data

    private String removeUnwantedSpaces(String contents) {
        if(contents.charAt(0) == '﻿') {
            contents = contents.substring(1);
        }
        if(contents.charAt(contents.length()-1) == '﻿'){
            contents = contents.substring(0, contents.length()-2);
        }
        return contents;
    }

    public List<DrillingBigData> mapCsvToObjects(String filePath) {
        CSVReader reader;
        HashMap<String, Integer> headerMap = new HashMap<>();
        List<DrillingBigData> drillingAttrsList = new ArrayList<>();
        try {
            reader = new CSVReader(new FileReader(filePath));
            int i = 0;
            DrillingBigData eachDrillingAttr = null;
            for (String[] strings : reader.readAll()) {
                if (i == 0) {
                    for (int j = 0; j < strings.length; j++) {
                        String contents = strings[j].trim();
                        if (headersReq.contains(contents)) {
                            headerMap.put(contents, j);
                        }
                    }
                }
                if (i > 0) {
                    eachDrillingAttr = new DrillingBigData();
                    eachDrillingAttr.setSslipdx(strings[headerMap.get("SSLIPDX")]);
                    eachDrillingAttr.setRpmax(strings[headerMap.get("RPMAX")]);
                    eachDrillingAttr.setWobax(strings[headerMap.get("WOBAX")]);
                    eachDrillingAttr.setSs(strings[headerMap.get("SS")]);
                    eachDrillingAttr.setTorax(strings[headerMap.get("TORAX")]);
                    eachDrillingAttr.setCopMse(strings[headerMap.get("COP_MSE")]);
                    drillingAttrsList.add(eachDrillingAttr);
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return drillingAttrsList;
    }

    public List<DrillingLowData> mapCsvToObjectsLowData(String filePath) {
        CSVReader reader;
        HashMap<String, Integer> headerMap = new HashMap<>();
        List<DrillingLowData> drillingAttrsList = new ArrayList<>();
        try {
            reader = new CSVReader(new FileReader(filePath));
            int i = 0;
            DrillingLowData eachDrillingAttr = null;
            for (String[] strings : reader.readAll()) {
                if (i == 0) {
                    for (int j = 0; j < strings.length; j++) {
                        String contents = strings[j].trim();
                        contents = removeUnwantedSpaces(contents);
                        if (headersReq.contains(contents)) {
                            headerMap.put(contents, j);
                        }
                    }
                }
                if (i > 0) {
                    eachDrillingAttr = new DrillingLowData();
                    eachDrillingAttr.setSslipdx(strings[headerMap.get("SSLIPDX")]);
                    eachDrillingAttr.setRpmax(strings[headerMap.get("RPMAX")]);
                    eachDrillingAttr.setWobax(strings[headerMap.get("WOBAX")]);
                    eachDrillingAttr.setSs(strings[headerMap.get("SS")]);
                    eachDrillingAttr.setTorax(strings[headerMap.get("TORAX")]);
                    eachDrillingAttr.setCopMse(strings[headerMap.get("COP_MSE")]);
                    drillingAttrsList.add(eachDrillingAttr);
                }
                i++;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return drillingAttrsList;
    }


}
