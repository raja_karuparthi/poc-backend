package com.poc.drilling.serviceImpl;

import com.poc.drilling.Msc.StoreDataInput;
import com.poc.drilling.jpa.DrillingBigRepository;
import com.poc.drilling.jpa.DrillingLowRepository;
import com.poc.drilling.jpa.RadarPlotRepository;
import com.poc.drilling.model.*;
import com.poc.drilling.service.DrillingDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DrillingDataServiceImpl implements DrillingDataService {

    private static Integer initialVal = 1;

    private static String massDataFile = "AllData.csv";

    private static String lessDataFile = "DrillingDataDashboard.csv";

    private static String radarFile = "LimiterRadar.csv";

    @Autowired
    private DrillingBigRepository drillingBigRepository;

    @Autowired
    private DrillingLowRepository drillingLowRepository;

    @Autowired
    private RadarPlotRepository radarPlotRepository;

    @Override
    public int storeDrillingData(StoreDataInput input) {

        CsvReaderImpl csvReader = new CsvReaderImpl();
        if (input.getQuantity().equalsIgnoreCase("high")) {
            List<DrillingBigData> drillingData = csvReader.mapCsvToObjects(massDataFile);
            drillingData = drillingData.subList(1, 10000);
            try {
                drillingBigRepository.saveAll(drillingData);
                return drillingData.size();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        } else if (input.getQuantity().equalsIgnoreCase("low")) {
            List<DrillingLowData> drillingData = csvReader.mapCsvToObjectsLowData(lessDataFile);
            try {
                drillingLowRepository.saveAll(drillingData);
                return drillingData.size();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    @Override
    public int storeRadarPlotData() {
        CsvReaderImpl csvReader = new CsvReaderImpl();
        List<RadarPlot> radarPlotData = csvReader.getRadarPlotData(radarFile);
        try {
            radarPlotRepository.saveAll(radarPlotData);
            return radarPlotData.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    @Override
    public List<CalculateCount> calculateCount() {
        Iterable<DrillingBigData> attrsIterable = drillingBigRepository.findAll();
        return calculateCountOfEachSslipBigData(attrsIterable);
    }

    @Override
    public List<CalculateCount> calculateCountOfEachSslipLowData() {
        Map<String, List<WobaxRpm>> slipdxMap = new HashMap<>();
        for (DrillingLowData drillingAttrs : drillingLowRepository.findAll()) {
            String sslipdx = String.valueOf(drillingAttrs.getSslipdx());
            if (slipdxMap.containsKey((sslipdx))) {
                List<WobaxRpm> wobaxRpmList = slipdxMap.get(sslipdx);
                wobaxRpmList.add(new WobaxRpm(drillingAttrs.getWobax(), drillingAttrs.getRpmax()));
                slipdxMap.put(sslipdx, wobaxRpmList);
            } else {
                List<WobaxRpm> list = new ArrayList<>(Arrays.asList(new WobaxRpm(drillingAttrs.getWobax(), drillingAttrs.getRpmax())));
                slipdxMap.put(sslipdx, list);
            }
        }
        List<CalculateCount> list = new ArrayList<>();
        for (String key: slipdxMap.keySet()){
            list.add(new CalculateCount(key, slipdxMap.get(key), slipdxMap.get(key).size()));
        }
        System.out.println(list);
        return list;
    }

    @Override
    public List<DrillingLowData> getLowDrillingAttrs() {
        List<DrillingLowData> list = new ArrayList<>();
        for (DrillingLowData drillingAttrs : drillingLowRepository.findAll()) {
            list.add(drillingAttrs);
        }
        return list;
    }

    @Override
    public List<DrillingBigData> getHighDrillingAttrs() {
        List<DrillingBigData> list = new ArrayList<>();
        for (DrillingBigData drillingAttrs : drillingBigRepository.findAll()) {
            list.add(drillingAttrs);
        }
        return list.subList(0, 9999);

    }

    @Override
    public List<RadarPlot> getAllRadarData() {
        List<RadarPlot> list = new ArrayList<>();
        Iterator<RadarPlot> radarPlotIterator = radarPlotRepository.findAll().iterator();
        while (radarPlotIterator.hasNext()) {
            list.add(radarPlotIterator.next());
        }
        return list;
    }

    @Override
    public void deleteAll(StoreDataInput quantity) {
        if (quantity.getQuantity().equalsIgnoreCase("high")) {
            drillingBigRepository.deleteAll();
        }
        else if(quantity.getQuantity().equalsIgnoreCase("low")) {
            drillingLowRepository.deleteAll();
        }
    }

    @Override
    public void deleteRadardata() {
        radarPlotRepository.deleteAll();
    }

    private List<CalculateCount> calculateCountOfEachSslipBigData(Iterable<DrillingBigData> attrsIterable) {
        Map<String, List<WobaxRpm>> slipdxMap = new HashMap<>();
        for (DrillingBigData drillingAttrs : attrsIterable) {
            String sslipdx = String.valueOf(drillingAttrs.getSslipdx());
            if (slipdxMap.containsKey((sslipdx))) {
                List<WobaxRpm> wobaxRpmList = slipdxMap.get(sslipdx);
                wobaxRpmList.add(new WobaxRpm(drillingAttrs.getWobax(), drillingAttrs.getRpmax()));
                slipdxMap.put(sslipdx, wobaxRpmList);
            } else {
                List<WobaxRpm> list = new ArrayList<>(Arrays.asList(new WobaxRpm(drillingAttrs.getWobax(), drillingAttrs.getRpmax())));
                slipdxMap.put(sslipdx, list);
            }
        }
        List<CalculateCount> list = new ArrayList<>();
        for (String key: slipdxMap.keySet()){
            list.add(new CalculateCount(key, slipdxMap.get(key), slipdxMap.get(key).size()));
        }
        System.out.println(list);
        return list;
    }
}
