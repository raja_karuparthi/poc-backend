package com.poc.drilling.service;

import com.poc.drilling.Msc.StoreDataInput;
import com.poc.drilling.model.CalculateCount;
import com.poc.drilling.model.DrillingBigData;
import com.poc.drilling.model.DrillingLowData;
import com.poc.drilling.model.RadarPlot;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "/api")
public interface DrillingDataService {

    @RequestMapping(path = "/store-drilling-data", method = RequestMethod.POST)
    int storeDrillingData(@RequestBody StoreDataInput quantity);

    @RequestMapping(path = "/store-radar-data", method = RequestMethod.GET)
    int storeRadarPlotData();

    @RequestMapping(path = "/calculate-sslipdx-big", method = RequestMethod.GET)
    List<CalculateCount> calculateCount();

    @RequestMapping(path = "/calculate-sslipdx-low", method = RequestMethod.GET)
    List<CalculateCount> calculateCountOfEachSslipLowData();

    @RequestMapping(path = "/get-drilling-data", method = RequestMethod.GET)
    List<DrillingBigData> getHighDrillingAttrs();

    @RequestMapping(path = "/get-few-drilling-data", method = RequestMethod.GET)
    List<DrillingLowData> getLowDrillingAttrs();

    @RequestMapping(path = "/get-all-radar-data", method = RequestMethod.GET)
    List<RadarPlot> getAllRadarData();

    @RequestMapping(path = "/delete-all", method = RequestMethod.DELETE)
    void deleteAll(@RequestBody StoreDataInput quantity);

    @RequestMapping(path = "/delete-radar-data", method = RequestMethod.DELETE)
    void deleteRadardata();




}
